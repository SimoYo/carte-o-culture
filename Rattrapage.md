

# Fiche technique
L’objectif de cette itération est de créer une feature permettant à l’utilisateur de recevoir une playlist musicale correspondant à un mot entré en paramètre.
La playlist Comportera 20 titres ayant tous mot entré en paramètre en commun dans leur titre.
L’objectif de cette feature dans une version ultérieure sera de permettre à l’utilisateur d’accéder à une playlist des chansons les plus populaires ayant le nom d’une ville ou d’un pays dans leur titre.


Cette feature utilise l'api Spotify pour extraîre les titres et créer les playlists.

Voici une description des différentes classes et fonctions implémentées.

## Classe `ClientCredentials`

Cette classe est utilisée pour obtenir et gérer les informations d'authentification (les jetons d'accès) à l'API Spotify. Elle utilise le client Spotify fourni par la bibliothèque `se.michaelthelin.spotify`.

Elle contient deux méthodes principales pour obtenir des jetons d'accès : `clientCredentials_Sync()` et `clientCredentials_Async()`.

### `clientCredentials_Sync()`

Cette méthode est utilisée pour obtenir un jeton d'accès de manière synchronisée. C'est-à-dire que le programme attend la réponse du serveur Spotify avant de continuer. Cette méthode pourrait bloquer l'exécution du programme si la demande d'accès prend beaucoup de temps. Si la demande réussit, le jeton d'accès est mis à jour pour l'objet `spotifyApi`, et le délai d'expiration du jeton est affiché.

### `clientCredentials_Async()`

Cette méthode est utilisée pour obtenir un jeton d'accès de manière asynchrone. C'est-à-dire que le programme ne bloquera pas en attendant la réponse du serveur Spotify. Au lieu de cela, il peut continuer à effectuer d'autres tâches. Une fois la réponse reçue, le programme revient et traite la réponse. Comme dans la méthode synchronisée, si la demande réussit, le jeton d'accès est mis à jour pour l'objet `spotifyApi`, et le délai d'expiration du jeton est affiché.

### `main()`

En plus de ces deux méthodes, la classe a une méthode `main()`. Cette méthode est le point d'entrée du programme, et elle appelle simplement les deux méthodes `clientCredentials_Sync()` et `clientCredentials_Async()` pour démontrer leur utilisation.

## Classe `SpotifyPlaylistSearch`

Cette classe est utilisée pour interagir avec l'API Spotify afin de rechercher des chansons sur Spotify en fonction d'un terme de recherche.

### `CLIENT_ID` et `CLIENT_SECRET`

Ces variables stockent les informations d'identification de l'API Spotify. Elles doivent être remplacées par vos propres informations d'identification Spotify.

### `main(String[] args)`

Cette méthode crée une instance de l'API Spotify, récupère les informations d'identification du client, effectue une recherche de chansons sur Spotify en utilisant un terme de recherche spécifié et affiche les chansons trouvées.

Dans cette méthode :

- Une instance de `SpotifyApi` est créée en utilisant le `CLIENT_ID` et le `CLIENT_SECRET`.
- Un `ClientCredentialsRequest` est construit et exécuté pour obtenir un jeton d'accès à l'API Spotify. Ce jeton d'accès est ensuite utilisé pour toutes les futures interactions avec l'API Spotify.
- Un terme de recherche est spécifié, dans ce cas, "mot_a_rechercher".
- Un `SearchTracksRequest` est créé et exécuté pour rechercher des chansons sur Spotify qui correspondent au terme de recherche. La recherche est limitée à 20 résultats.
- Les chansons trouvées sont affichées sur la console.


## Problèmes rencontrés

Je n'ai pas eu le temps de régler tous les problèmes liés aux dépendances, Il reste une erreur Exception in thread "main" java.lang.NoSuchMethodError: 'com.google.gson.JsonElement com.google.gson.JsonParser.parseString(java.lang.String)' à la compilation.


# Intégration continue

Travis est un service d’intégration continue, il est utilisé dans les tests et le déploiement d’un projet. Il sert notamment à évaluer la qualité du code.

Voici son fonctionnement:
Le fichier travis.yml contient les instructions. On le crée à la racine du projet en y spécifiant les langages de programmation.
Travis va détecter les erreurs en installant les dépendances et en exécutant les commandes de test et de construction du fichier travis.yml. Travis va commenter les tests qui ont échoué.

