package src.app.service;

import java.util.List;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import src.app.entity.Marqueur;
import src.app.repository.MarqueurRepository;

@Service
@Transactional
public class MarqueurService {
    // Attribut
    @Autowired private MarqueurRepository repo;

    // Définition des méthodes CRUD

    // Définition des méthodes CREATE
    public void save(List<Marqueur> marqueurs) {
        for (Marqueur marqueur : marqueurs) {
            if(this.repo.findByIDBookAndIDVille(marqueur.getIDBook(),marqueur.getIDVille()) == 0) {
                this.repo.save(marqueur);
            }
        }
    }

    // Définition des méthodes READ
    public List<Marqueur> getAll() {
        return this.repo.findAll();
    }

    public Marqueur get(Integer id) {
        return this.repo.findById(id).get();
    }
    
    // Définition des méthodes UPDATE

    // Définition des méthodes DELETE

    // Autre methodes
}