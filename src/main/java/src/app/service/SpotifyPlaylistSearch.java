package src.app.service;


import org.apache.hc.core5.http.ParseException;
import se.michaelthelin.spotify.SpotifyApi;
import se.michaelthelin.spotify.exceptions.SpotifyWebApiException;
import se.michaelthelin.spotify.model_objects.credentials.ClientCredentials;
import se.michaelthelin.spotify.model_objects.specification.Paging;
import se.michaelthelin.spotify.model_objects.specification.Track;
import se.michaelthelin.spotify.requests.authorization.client_credentials.ClientCredentialsRequest;
import se.michaelthelin.spotify.requests.data.search.simplified.SearchTracksRequest;

import java.io.IOException;

public class SpotifyPlaylistSearch {

    // Remplacez les valeurs ci-dessous par vos propres informations d'identification Spotify
    private static final String CLIENT_ID = "789612051437420daf3201604138b701";
    private static final String CLIENT_SECRET = "ee7b003b2c4e4007a824e91682e71c38";

    public static void main(String[] args) {
        // Création de l'API Spotify
        SpotifyApi spotifyApi = new SpotifyApi.Builder()
                .setClientId(CLIENT_ID)
                .setClientSecret(CLIENT_SECRET)
                .build();

        // Récupération des informations d'identification client
        ClientCredentialsRequest clientCredentialsRequest = spotifyApi.clientCredentials()
                .build();

        try {
            ClientCredentials clientCredentials = clientCredentialsRequest.execute();
            spotifyApi.setAccessToken(clientCredentials.getAccessToken());

            // Mot à rechercher dans le titre des titres
            String searchTerm = "mot_a_rechercher";

            // Recherche des titres correspondants
            SearchTracksRequest searchTracksRequest = spotifyApi.searchTracks(searchTerm)
                    .limit(20) // Limite le nombre de résultats à 20
                    .build();

            Paging<Track> trackPaging = searchTracksRequest.execute();
            Track[] tracks = trackPaging.getItems();

            // Affichage des titres
            System.out.println("Playlist des titres les plus écoutés avec le mot \"" + searchTerm + "\":");
            for (Track track : tracks) {
                System.out.println(track.getName());
            }

        } catch (IOException | SpotifyWebApiException e) {
            System.out.println("Une erreur s'est produite : " + e.getMessage());
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public static void getPlaylistByMot(String mot) {
    }
}

