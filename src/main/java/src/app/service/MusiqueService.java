package src.app.service;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import src.app.models.Genius;
import src.app.models.Musique;
import fr.france.*;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class MusiqueService {
    static Genius gla = new Genius();

    public ResponseEntity<Musique> getLyrics(String titre) throws IOException {
        Musique m = new Musique(gla.search(titre).getHits().get(0).fetchLyrics());
        List<String> commune = RepertoireCommune.getCommunes().stream().map(Commune::getNom).collect(Collectors.toList());
        Set<String> listeDeMots = new HashSet<>();
        String[] mots = m.getParole().split("\\W+");
        for (String mot : mots) {
            listeDeMots.add(mot.toUpperCase());
        }
        for (String mot : listeDeMots) {
            if (commune.contains(mot)) { // Vérifie si le mot est présent dans la liste de référence, en ignorant la casse
                m.setElement(mot+", "+m.getElement());
            }
        }
        return new ResponseEntity<Musique> (m, HttpStatus.OK);
    }
}
