package src.app.repository;

import java.util.List;

import src.app.entity.Marqueur;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MarqueurRepository extends JpaRepository<Marqueur, Integer> {
    @Query("SELECT COUNT(m) FROM Marqueur m WHERE m.id_book = :id_book AND m.id_ville = :id_ville")
    Integer findByIDBookAndIDVille(@Param("id_book") Integer id_book, @Param("id_ville") Integer id_ville);
}