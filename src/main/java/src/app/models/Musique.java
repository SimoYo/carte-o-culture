package src.app.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Musique {
    String parole;
    String element;
    public Musique(String parole){
        this.parole=parole;
        this.element="";
    }
}
