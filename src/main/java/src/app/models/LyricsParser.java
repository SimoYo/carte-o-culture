package src.app.models;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public class LyricsParser {

    public LyricsParser() {
    }

    public String get(String id) {
        return parseLyrics(id);
    }

    private String parseLyrics(String id) {
        try {
            URLConnection connection = new URL("https://genius.com/songs/" + id + "/embed.js").openConnection();
            connection.setRequestProperty("User-Agent", "");
            Scanner scanner = new Scanner(connection.getInputStream());
            scanner.useDelimiter("\\A");
            String raw = "";
            while (scanner.hasNext()) {
                raw += scanner.next();
            }
            if (raw.equals("")) {
                return null;
            }
            return getReadable(raw);
        } catch (IOException e) {
            return null;
        }
    }

    private String getReadable(String rawLyrics) {
        //Remove start
        rawLyrics = rawLyrics.replaceAll("[\\S\\s]*<div class=\\\\\\\\\\\\\"rg_embed_body\\\\\\\\\\\\\">[ (\\\\\\\\n)]*", "");
        //Remove end
        rawLyrics = rawLyrics.replaceAll("[ (\\\\\\\\n)]*<\\\\/div>[\\S\\s]*", "");
        //Remove tags between
        rawLyrics = rawLyrics.replaceAll("<[^<>]*>", "");
        //Unescape spaces
        rawLyrics = rawLyrics.replaceAll("\\\\\\\\n","\n");
        //Unescape '
        rawLyrics = rawLyrics.replaceAll("\\\\'", "'");
        //Unescape "
        rawLyrics = rawLyrics.replaceAll("\\\\\\\\\\\\\"", "\"");
        return rawLyrics;
    }
}