package src.app.models;

import java.io.IOException;

public class Genius {

    private HttpManager httpManager = new HttpManager();

    public SongSearch search(String query) throws IOException {
        return new SongSearch(this, query);
    }

    public HttpManager getHttpManager() {
        return this.httpManager;
    }
}
