package src.app.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import src.app.entity.Page;
import src.app.entity.Chapitre;
import src.app.entity.Livre;
import src.app.entity.Marqueur;
import src.app.entity.Ville;
import src.app.service.MarqueurService;
import src.app.service.VilleService;

 @RestController
 public class VilleController {
    // Atributes
    @Autowired private VilleService service;

    // Constructeur
    public void VilleController() {
        this.service = new VilleService();
    }

    // RESTful API methods for READ operations
    @GetMapping("/api/villes/")
    public ResponseEntity<List<Ville>> getAll() {
        try {
            List<Ville> villes = this.service.getAll();
            return new ResponseEntity<List<Ville>>(villes, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<List<Ville>>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/api/villes/{id}")
    public ResponseEntity<Ville> get(@PathVariable Integer id) {
        try {
            Ville ville = this.service.get(id);
            return new ResponseEntity<Ville>(ville, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Ville>(HttpStatus.NOT_FOUND);
        }
    }
    
    // RESTful API method for CREATE operation
    @PostMapping("/api/villes/add")
    public Integer addNewVille (@RequestParam String nom, @RequestParam Float x, @RequestParam Float y) {
        Ville v = new Ville();
        v.setNom(nom);
        v.setLatitude(x);
        v.setLongitude(y);
        return this.service.save(v).getID();
    }

    // RESTful API method for UPDATE operation
    
    // RESTful API method for DELETE operation

    // Autres methodes        
    public List<Marqueur> getLieuxFromLivre(Livre l) {
        // On recupere le texte entier
        String texte = ""; ArrayList<Marqueur> marqueurs = new ArrayList<Marqueur>();
        for (Chapitre chapter : l.getChapters()) {
            for (Page page : chapter.getpages()) {
                texte = page.gettexte();

                // On recherche les mots contenant une lttre majuscule
                for(String mot : Arrays.asList(texte.split(" "))) {
                    if(mot.matches("[A-Z][a-z]*")) {
                        Integer id_ville = this.service.isLieux(mot);
                        if(id_ville != -1) {
                            Marqueur temp = new Marqueur(l.getid_book(), id_ville);
                            marqueurs.add(temp);
                            texte = texte.replaceAll(mot,"<a href='/api/villes/"+id_ville+"'>"+mot+"</a>");
                        }
                    }
                }

                page.settexte(texte);
            }
        }
        return marqueurs;
    }
}
