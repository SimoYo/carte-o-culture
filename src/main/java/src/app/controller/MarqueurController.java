package src.app.controller;

import java.util.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import src.app.entity.Marqueur;
import src.app.service.MarqueurService;

@RestController
public class MarqueurController {
       // Atributs
       @Autowired
       private MarqueurService service;
   
       // RESTful API methods for READ operations
       @GetMapping("/api/marqueur/")
       public ResponseEntity<List<Marqueur>> getAll() {
           try {
               List<Marqueur> marqueurs = this.service.getAll();
               return new ResponseEntity<List<Marqueur>>(marqueurs, HttpStatus.OK);
           } catch (NoSuchElementException e) {
               return new ResponseEntity<List<Marqueur>>(HttpStatus.NOT_FOUND);
           }
       }
   
       @GetMapping("/api/marqueur/{id}")
       public ResponseEntity<Marqueur> get(@PathVariable Integer id) {
           try {
            Marqueur marqueur = this.service.get(id);
               return new ResponseEntity<Marqueur>(marqueur, HttpStatus.OK);
           } catch (NoSuchElementException e) {
               return new ResponseEntity<Marqueur>(HttpStatus.NOT_FOUND);
           }
       }
   
       // RESTful API method for CREATE operation
       @PostMapping("/api/marqueur/add")
       public void add(@RequestBody List<Marqueur> marqueurs) {
           this.service.save(marqueurs);
       }
   
       // RESTful API method for UPDATE operation

   
       // RESTful API method for DELETE operation
}
