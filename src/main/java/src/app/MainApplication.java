package src.app;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.nio.file.Files;
import java.io.IOException;
import static java.nio.file.StandardCopyOption.*;

import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.beans.factory.annotation.Autowired;

import src.app.entity.*;
import src.app.models.Ebook;
import src.app.controller.*;
import src.app.service.MusiqueService;
import src.app.service.SpotifyPlaylistSearch;


@Controller
@SpringBootApplication
public class MainApplication {
	// Controllers
	@Autowired private LivreController livreAPI;
	@Autowired private VilleController villeAPI;
	@Autowired private MarqueurController marqueurAPI;
	@Autowired private MusiqueService musiqueService;
	private EBookController ebookAPI = new EBookController();
	private PageController  pageBUILDER = new PageController();

	// Lancement de l'application
	public static void main(String[] args) {
		SpringApplication.run(MainApplication.class, args);
	}

	// Rendement des pages

	// Rendement de la page d'acceuil
	@GetMapping("")
	public String getIndex(Model m) {
		return "index";
	}

	// Rendement des pages de livre
	@GetMapping("livres")
	public String getLivreAll(Model m) {
		try {
			m.addAttribute("livres", livreAPI.getAll().getBody());
			return "book/book-display-all";
		} catch (Exception e) {
			return "error";
		}
    }

	@GetMapping("musique/{titre}")
	public String getMusiqueByTitre(Model m,@PathVariable String titre) {
		try {
			m.addAttribute("musique", musiqueService.getLyrics(titre).getBody());
			return "book/musique-info";
		} catch (Exception e) {
			return "error";
		}
	}

	// Rendemement de la page d'information d'un livre
	@GetMapping("livres/{id}")
	public String getLivreById(Model m, @PathVariable Integer id) {
		try {
			m.addAttribute("livre", livreAPI.get(id).getBody());
			return "book/book-display-info";
		} catch (Exception e) {
            return "error";
        }
    }

	// Rendemement de la page de lecture d'un livre
	@GetMapping("livres/{id}/read")
	public String readLivre(Model m, @PathVariable Integer id) {
		try {
			m.addAttribute("livre", livreAPI.get(id).getBody());
			return "book/book-read";
		} catch (Exception e) {
			return "error";
		}
	}



	@GetMapping("spotify/{mot}")
	public String getPlaylistByMot(Model m,@PathVariable String mot) {
		try {
			SpotifyPlaylistSearch.getPlaylistByMot(mot);
			return "book/spotify-playlist";
		} catch (Exception e) {
			return "error";
		}
	}





	// Rendemement de la page d'ajout d'un livre
	@GetMapping("livres/add")
	public String addLivreForm(Model m) {
		m.addAttribute("ebook", new EBookController());
		return "book/book-form-add";
    }

	// Traitement de la requète d'ajout d'un livre
	@PostMapping("livres/add")
	public String addLivreRequest(@ModelAttribute Ebook ebook) {
		try {
			if(ebook.isEmpty() == false) {
				System.out.println("Nouveau Livre Détecté");
				Livre l = ebookAPI.loadDataFromEbook(ebook);
				livreAPI.add(l);
				List<Marqueur> marqueurs = villeAPI.getLieuxFromLivre(l);
				System.out.println(marqueurs);
				marqueurAPI.add(marqueurs);
				//List<String> names = new ArrayList<String>(Arrays.asList(l.getTexte().split(",")));
				//System.out.println(this.villeAPI.getFiltered(names));
				Path path1 = Paths.get("./src/main/resources/static/img/cover/temp.jpg");
				Path path2 = Paths.get("./src/main/resources/static/img/cover/"+l.getid_book()+".jpg");
				if(l.getid_book() > 0) {
					Files.move(path1, path2, REPLACE_EXISTING);
				} else {
					Files.delete(path1);
				}
				System.out.println("Nouveau Livre Ajouté");
				return("redirect:/livres");
			} else {
				System.out.println("Aucun Livre Envoyé");
				return("redirect:/livres/add");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "error";
	}

	// Rendement de la page de carte
	@GetMapping("carte")
	public String getCarte(Model model) {
		ArrayList<Ville> villes = new ArrayList<Ville>();
		for (Marqueur marqeur : marqueurAPI.getAll().getBody()) {
			Ville ville = villeAPI.get(marqeur.getIDVille()).getBody();
			villes.add(ville);
		}

		System.out.println(villes);


		model.addAttribute("villes", villes);
		model.addAttribute("livres", livreAPI.getAll().getBody());
        return("map/map-display");
    }





	// Rendement de la page de test
	@GetMapping("test")
	public String getTest(Model m) {
		m.addAttribute("livre", livreAPI.get(0));
		m.addAttribute("page_content", pageBUILDER);
		return pageBUILDER.hasBody("<div th:replace='book/book-display-all::copy'></div>").build();
		//!\\ bloque au render du thymeleaf du builder //!\\
	}
}
