package src.app.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="marqueur")
public class Marqueur {
    // Attributs
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer id_book;
    private Integer id_ville;

    // Constructeurs
    public Marqueur() {}

    public Marqueur(Integer _id_book, Integer _id_ville) {
        this.id  = null;
        this.id_book = _id_book;
        this.id_ville  = _id_ville;
    }

    // Getters et Setters 
    public Integer getID() { return this.id; }
    public Integer getIDBook() { return this.id_book; }
    public Integer getIDVille() { return this.id_ville; }

    public void setIDBook(Integer x)  { this.id_book=x; }
    public void setIDVille(Integer x) { this.id_ville=x; }

    // Méthodes
    public String toString() {
        return Integer.toString(this.id_book)+" "+Integer.toString(this.id_ville);
    }

}