   var carte = null;

    function initMap() {
        let center = [48.852969,2.349903];
        carte = L.map('map').setView([lat, lon], 11);
        L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
            minZoom: 1,
            maxZoom: 20
        }).addTo(carte);
    }

    function addMarker() {
        let lat = 48.852969;
        let lon = 2.349903;
        L.marker([lat, lon]).addTo(carte);
    }

    // Launching
    window.onload = function(){
        initMap(); 
        addMarker();
        addMarker();
    };