
var map;
var markers = [];

// Map functions
function create_map(div_name) {
    var mapProp= {
        center:new google.maps.LatLng(51.508742,-0.120850),
        zoom:5,
    };
    map = new google.maps.Map(document.getElementById("map"),mapProp);
    return map;
}

// Marker functions
function create_marker(x, y, name, _label, color, map) {
    var LatLng = {lat: x, lng: y};
    console.log(color);
    marker = new google.maps.Marker({
        position: LatLng,
        title: name,
        label: _label,
        icon: {
            url: "http://maps.google.com/mapfiles/ms/icons/"+color+"-dot.png",
            scaledSize: new google.maps.Size(75, 75),
        },
    });
    marker.setMap(map);
    marker.setVisible(true);
    return marker;
}

function initMap() {
    create_map("map");
    var marker;	var i=0;
    var colors = ["blue","red","yellow","orange","green","blue","red","yellow","orange","green"];
    var livres = $(".livre");
    var i = 0;
    livres.each(function() {
        var ville = this.innerHTML.split("|");
        ville[1] = parseFloat(ville[1]);
        ville[2] = parseFloat(ville[2]);
        console.log(ville)
        marker = create_marker(ville[1],ville[2],ville[0],i.toString(),"orange",map);
        //markers[i].push(marker)
        i=i+1;
    })						
}

function toggle_marker(box,i) {
    if (box.checked) {
        console.log(markers[i]);
        for(let j=0; j < markers[i].length; j++) {
            markers[i][j].setVisible(true);
        }
    } else {
        for(let j=0; j < markers[i].length; j++) {
            markers[i][j].setVisible(false);
        }
    }
}
